#include "cells.hpp"
#include "world.hpp"

char Cell::display() { return '#'; }

int Cell::eats() { return 1; }

void Cell::tick(World *world, int i, int j) {
  ;  // Conway's cell does nothing on world tick
}

bool Cell::will_live(int neighbors) {
  return (neighbors == 2) || (neighbors == 3);
}

bool Cell::will_grow(int neighbors) { return neighbors == 3; }

char TimerCell::display() {
  return this->timer + '0';  // show remaining ticks
}

int TimerCell::eats() { return 10; }

void TimerCell::tick(World *world, int i, int j) { timer--; }

bool TimerCell::will_live(int neighbors) {
  return timer > 0;  // dies only when timer is out
}

bool TimerCell::will_grow(int neighbors) {
  return false;  // will never grow by itself
}

char DynamicCell::display() { return '*'; }

int DynamicCell::eats() { return 2; }

bool DynamicCell::will_grow(int neighbors) { return neighbors == 2; }

bool DynamicCell::will_live(int neighbors) {
  return (neighbors == 3) || (neighbors == 4);
}

char ProducerCell::display() { return 'P'; }

int ProducerCell::eats() { return 4; }

bool ProducerCell::will_grow(int neighbors) { return neighbors == 4; }

bool ProducerCell::will_live(int neighbors) { return neighbors == 4; }

void ProducerCell::tick(World *world, int i, int j) {
  world->put_food(i - 1, j, 1);
  world->put_food(i, j - 1, 1);
}

CellPool::CellPool()
    : regular_cell(nullptr), dynamic_cell(nullptr), producer_cell(nullptr) {}

CellPool *CellPool::instance = nullptr;

CellPool *CellPool::get_instance() {
  if (!instance) instance = new CellPool();
  return instance;
}

void CellPool::dispose() {
  if (instance) delete instance;
}

Cell *CellPool::get_regular_cell() {
  if (!regular_cell) regular_cell = new Cell();
  return regular_cell;
}

DynamicCell *CellPool::get_dynamic_cell() {
  if (!dynamic_cell) dynamic_cell = new DynamicCell();
  return dynamic_cell;
}

ProducerCell *CellPool::get_producer_cell() {
  if (!producer_cell) producer_cell = new ProducerCell();
  return producer_cell;
}

TimerCell *CellPool::get_timer_cell(int timer) {
  if (timer_cells.empty()) {
    return new TimerCell(timer);
  }
  TimerCell *cell = timer_cells.back();
  timer_cells.pop_back();
  cell->reset(timer);
  return cell;
}

void CellPool::return_timer_cell(TimerCell *tc) { timer_cells.push_back(tc); }

CellPool::~CellPool() {
  if (regular_cell) delete regular_cell;
  if (dynamic_cell) delete dynamic_cell;
  if (producer_cell) delete producer_cell;
  for (TimerCell *tc : timer_cells) delete tc;
}

Cell *CellFactory::make_cell(char id, int timer) {
  if (id == '#' || id == 'C' || id == 'c')
    return CellPool::get_instance()->get_regular_cell();
  if (id == '*' || id == 'D' || id == 'd')
    return CellPool::get_instance()->get_dynamic_cell();
  if (id == 'P' || id == 'p')
    return CellPool::get_instance()->get_producer_cell();
  if (id >= '0' && id <= '9')
    return CellPool::get_instance()->get_timer_cell(id - '0');
  if (id == 'T' || id == 't' || id == 'O')
    return CellPool::get_instance()->get_timer_cell(timer);
  return nullptr;
}

Cell *CellFactory::make_cell(int id, int timer) {
  if (id == 0) return CellPool::get_instance()->get_regular_cell();
  if (id == 1) return CellPool::get_instance()->get_dynamic_cell();
  if (id == 2) return CellPool::get_instance()->get_producer_cell();
  if (id == 3) return CellPool::get_instance()->get_timer_cell(timer);
  return nullptr;
}