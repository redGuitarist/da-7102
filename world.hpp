#pragma once

#include <string>
#include <vector>
#include "cells.hpp"

using std::string;
using std::vector;

class World {
 private:
  vector<vector<Cell*>> world, world2;
  CellFactory* cf;
  vector<vector<int>> foodmap;
  int ticks_elapsed;
  int width, height;
  void normalize(int& i, int& j) const {
    i %= height;
    j %= width;
    if (i < 0) i += height;
    if (j < 0) j += width;
  }
  int count_around(int i, int j) const;

 public:
  // construct empty world
  World(int width, int height);

  // construct random world with given cell density
  World(int height, int width, double density);

  // load world from file
  World(std::string filename);

  // return cell type at (i, j)
  char get_cell_at(int i, int j) const {
    normalize(i, j);
    if (world[i][j])
      return world[i][j]->display();
    else
      return ' ';
  }

  // return amount of food at (i, j)
  int get_food_at(int i, int j) const {
    normalize(i, j);
    return foodmap[i][j];
  }

  // get world age in ticks
  int get_ticks() const { return ticks_elapsed; }

  // add cell to world
  void put_cell(int i, int j, Cell* cell);

  // add food to cell at (i, j)
  void put_food(int i, int j, int food);

  // perform world tick
  void tick();

  // save world state to file
  void save_to_file(std::string filename);

  int get_width() const { return width; }

  int get_height() const { return height; }

  ~World() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        if (world[i][j] && world[i][j]->display() >= '0' &&
            world[i][j]->display() <= '9')
          CellPool::get_instance()->return_timer_cell(
              static_cast<TimerCell*>(world[i][j]));
      }
    }
    delete cf;
  }
};
