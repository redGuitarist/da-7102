package golife;

import java.util.ArrayList;

import golife.Cell;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.File;
import java.util.Scanner;
import java.util.Random;

public class World {
  private ArrayList<ArrayList<Cell>> world, world2;
  private ArrayList<ArrayList<Integer>> foodmap;
  private int ticks_elapsed = 0;
  private int width, height;

  private int normi(int i) {
    i %= height;
    if (i < 0)
      i += height;

    return i;
  }

  private int normj(int j) {
    j %= width;
    if (j < 0)
      j += width;

    return j;
  }

  private int count_around(int i, int j) {
    int cnt = 0;
    for (int a = -1; a <= 1; a++) {
      for (int b = -1; b <= 1; b++) {
        if (a == 0 && b == 0)
          continue;
        int ni = i + a;
        int nj = j + b;
        ni = normi(ni);
        nj = normj(nj);
        if (world.get(ni).get(nj) != null)
          cnt++;
      }
    }
    return cnt;
  }

  public World(int width, int height) {
    this.width = width;
    this.height = height;
    world = new ArrayList<>(height);
    world2 = new ArrayList<>(height);
    foodmap = new ArrayList<>(height);
    for (int i = 0; i < height; i++) {
      world.add(new ArrayList<Cell>(width));
      world2.add(new ArrayList<Cell>(width));
      foodmap.add(new ArrayList<Integer>(width));
      for (int j = 0; j < width; j++) {
        foodmap.get(i).add(0);
        world.get(i).add(null);
        world2.get(i).add(null);
      }
    }
  }

  public World(int width, int height, double density) {
    this(width, height);
    // get number of cells to generate
    int cells = (int) (width * height * density);
    // initialize random
    Random random = new Random(System.currentTimeMillis());

    // fill cells
    for (int c = 0; c < cells; c++) {
      // get random position
      int i;
      int j;
      do {
        i = random.nextInt(height);
        j = random.nextInt(width);
      } while (world.get(i).get(j) != null);

      int type = random.nextInt(3);
      Cell cell = null;
      switch (type) {
      case 0:
        cell = new Cell();
        break;
      case 1:
        cell = new DynamicCell();
        break;
      case 2:
        cell = new ProducerCell();
        break;
      }

      world.get(i).set(j, cell);
    }
  }

  public World(String filename) {
    try {
      Scanner file = new Scanner(new File(filename));
      width = file.nextInt();
      height = file.nextInt();

      world = new ArrayList<>(height);
      world2 = new ArrayList<>(height);
      foodmap = new ArrayList<>(height);
      for (int i = 0; i < height; i++) {
        world.add(new ArrayList<Cell>(width));
        world2.add(new ArrayList<Cell>(width));
        foodmap.add(new ArrayList<Integer>(width));
        for (int j = 0; j < width; j++) {
          foodmap.get(i).add(0);
          world.get(i).add(null);
          world2.get(i).add(null);
        }
      }

      ticks_elapsed = file.nextInt();

      for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
          char ch = file.next().charAt(0);
          if (ch == '#')
            world.get(i).set(j, new Cell());
          else if ('0' <= ch && ch <= '9')
            world.get(i).set(j, new TimerCell(ch - '0'));
          else if (ch == '*')
            world.get(i).set(j, new DynamicCell());
          else if (ch == 'P')
            world.get(i).set(j, new ProducerCell());
        }
      }

      for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
          foodmap.get(i).set(j, file.nextInt());
        }
      }
    } catch (Exception e) {
      System.out.println("Cannot read from file");
    }
  }

  public char get_cell_at(int i, int j) {
    i = normi(i);
    j = normj(j);
    if (world.get(i).get(j) != null)
      return world.get(i).get(j).display();
    else
      return ' ';
  }

  public int get_food_at(int i, int j) {
    i = normi(i);
    j = normj(j);
    return foodmap.get(i).get(j);
  }

  public int get_ticks() {
    return ticks_elapsed;
  }

  public void put_cell(int i, int j, Cell cell) {
    i = normi(i);
    j = normj(j);
    world.get(i).set(j, cell);
  }

  public void put_food(int i, int j, int food) {
    i = normi(i);
    j = normj(j);
    food += foodmap.get(i).get(j);
    food %= 10;
    foodmap.get(i).set(j, food);
  }

  public void tick() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        world2.get(i).set(j, null);
        int neighbors = count_around(i, j);
        if (world.get(i).get(j) == null) // cell is empty
        {
          if (ProducerCell.will_grow(neighbors))
            world2.get(i).set(j, new ProducerCell());
          else if (Cell.will_grow(neighbors))
            world2.get(i).set(j, new Cell());
          else if (DynamicCell.will_grow(neighbors))
            world2.get(i).set(j, new DynamicCell());
        } else {
          world.get(i).get(j).tick(this, i, j);
          if (world.get(i).get(j).will_live(neighbors))
            world2.get(i).set(j, world.get(i).get(j));
          else if (foodmap.get(i).get(j) >= world.get(i).get(j).eats()) {
            foodmap.get(i).set(j, foodmap.get(i).get(j) - world.get(i).get(j).eats());
            world2.get(i).set(j, world.get(i).get(j));
          } else {
            world2.get(i).set(j, null);
            foodmap.get(i).set(j, foodmap.get(i).get(j) + 1);
          }
        }
      }
    }

    // clean dead cells and copy back
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        world.get(i).set(j, world2.get(i).get(j));
      }
    }

    ticks_elapsed += 1;
  }

  public void save_to_file(String filename) {
    try {
      PrintWriter file = new PrintWriter(new FileWriter(filename));
      file.println(width);
      file.println(height);
      file.println(ticks_elapsed);
      for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
          if (get_cell_at(i, j) != ' ')
            file.print(get_cell_at(i, j) + " ");
          else
            file.print("- ");
        }
        file.println();
      }

      for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
          file.print(get_food_at(i, j) + " ");
        }
        file.println();
      }

      file.close();
    } catch (Exception e) {
      System.out.println("Unable to save to file");
    }
  }

  public int get_width() {
    return width;
  }

  public int get_height() {
    return height;
  }
}