package golife;

public class ProducerCell extends Cell {
  public static boolean will_grow(int neighbors) {
    return neighbors == 4;
  }

  @Override
  public boolean will_live(int neighbors) {
    return neighbors == 4;
  }

  @Override
  public char display() {
    return 'P';
  }

  @Override
  public int eats() {
    return 4;
  }

  @Override
  public void tick(World world, int i, int j) {
    world.put_food(i - 1, j, 1);
    world.put_food(i, j - 1, 1);
  }
}