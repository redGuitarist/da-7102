package golife;

import golife.World;

public class Cell {
  public static boolean will_grow(int neighbors) {
    return neighbors == 3;
  }

  public boolean will_live(int neighbors) {
    return (neighbors == 2) || (neighbors == 3);
  }

  public char display() {
    return '#';
  }

  public int eats() {
    return 1;
  }

  public void tick(World world, int i, int j) {
    ;
  }
}