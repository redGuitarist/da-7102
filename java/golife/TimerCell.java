package golife;

import golife.Cell;

public class TimerCell extends Cell {
  private int timer = 5;

  public static boolean will_grow(int neighbors) {
    return false;
  }

  @Override
  public boolean will_live(int neighbors) {
    return timer > 0;
  }

  @Override
  public char display() {
    return (char) (timer + (int) '0');
  }

  @Override
  public int eats() {
    return 10;
  }

  @Override
  public void tick(World world, int i, int j) {
    timer--;
  }

  public TimerCell(int timer) {
    this.timer = timer;
  }
}
