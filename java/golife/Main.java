package golife;

import java.util.Scanner;

import golife.*;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    World world;

    System.out.print("Load from file (empty - interactive): ");
    String fname = scanner.nextLine();

    if (fname.length() > 0) {
      world = new World(fname);
    } else {
      System.out.print("Enter width and height: ");
      int width = scanner.nextInt();
      int height = scanner.nextInt();

      System.out.print("Randomize density (0 for nonrandom): ");
      double dens = scanner.nextDouble();

      if (dens == 0.0)
        world = new World(width, height);
      else
        world = new World(width, height, dens);
    }

    System.out.println("Enter 'help' for command reference");
    while (true) {
      System.out.print(">> ");
      String cmd = scanner.next();

      if (cmd.equals("cells")) {
        for (int i = 0; i < world.get_height(); i++) {
          for (int j = 0; j < world.get_width(); j++)
            System.out.print(world.get_cell_at(i, j));

          System.out.println();
        }
      } else if (cmd.equals("foods")) {
        for (int i = 0; i < world.get_height(); i++) {
          for (int j = 0; j < world.get_width(); j++)
            System.out.print(world.get_food_at(i, j));

          System.out.println();
        }
      } else if (cmd.equals("cell")) {
        int i = scanner.nextInt(), j = scanner.nextInt();
        char type = scanner.next().charAt(0);

        switch (type) {
        case '#':
        case 'C':
        case 'c':
          world.put_cell(i, j, new Cell());
          break;
        case '*':
        case 'D':
        case 'd':
          world.put_cell(i, j, new DynamicCell());
          break;
        case 'P':
        case 'p':
          world.put_cell(i, j, new ProducerCell());
          break;
        case 'O':
        case 'T':
        case 't':
          int timer = scanner.nextInt();
          world.put_cell(i, j, new TimerCell(timer));
          break;
        }
      } else if (cmd.equals("tick")) {
        int ticks;
        if (scanner.hasNext())
          ticks = scanner.nextInt();
        else
          ticks = 1;

        for (int i = 0; i < ticks; i++) {
          System.out.println("Tick " + Integer.toString(i + 1));
          world.tick();
        }
      } else if (cmd.equals("ticks"))
        System.out.println(world.get_ticks());
      else if (cmd.equals("food")) {
        int i = scanner.nextInt(), j = scanner.nextInt(), food = scanner.nextInt();
        world.put_food(i, j, food);
      } else if (cmd.equals("clear")) {
        world = new World(world.get_width(), world.get_height());
      } else if (cmd.equals("exit"))
        break;
      else if (cmd.equals("save")) {
        String filename = scanner.next();
        world.save_to_file(filename);
      } else if (cmd.equals("help")) {
        System.out
            .println("help                        - show this\n" + "exit                        - stop the program\n"
                + "clear                       - make a new empty world\n"
                + "cells                       - show a current world state\n"
                + "foods                       - show a food map of the world\n"
                + "ticks                       - get a number of ticks since world " + "creation\n"
                + "tick [ticks]                - perform a number of world ticks, " + "default 1\n"
                + "save <filename>             - save the world to a file\n"
                + "food <i> <j> <amount>       - add food to cell at (i, j)\n"
                + "cell <i> <j> <type> [timer] - add a new cell at (i, j), types:\n"
                + "                              # - classic Conway cell\n"
                + "                                  (lives with 2, 3 neighbors, " + "born with 3,\n"
                + "                                   eats 1 food unit to not die)\n"
                + "                              * - dynamic cell\n"
                + "                                  (lives with 3, 4 neighbors, " + "born with 2,\n"
                + "                                   eats 2 food units to not " + "die)\n"
                + "                              P - producer cell\n"
                + "                                  (lives with 4 neighbors, " + "born with 4,\n"
                + "                                   eats 4 food units to not " + "die)\n"
                + "                              T - timer cell with specified " + "timer\n"
                + "                                  (lives exactly 'timer' ticks,\n"
                + "                                  'timer' propery is ignored for " + "other cells)\n");
      }

      System.out.println();
    }
  }
}