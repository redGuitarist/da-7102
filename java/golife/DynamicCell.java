package golife;

public class DynamicCell extends Cell {
  public static boolean will_grow(int neighbors) {
    return neighbors == 2;
  }

  @Override
  public boolean will_live(int neighbors) {
    return (neighbors == 3) || (neighbors == 4);
  }

  @Override
  public char display() {
    return '*';
  }

  @Override
  public int eats() {
    return 2;
  }
}