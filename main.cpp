#include <iostream>
#include <sstream>
#include <string>

#include "cells.hpp"
#include "world.hpp"

using namespace std;

int main(int argc, char** argv) {
  cout << "Load from file (empty - interactive): ";
  string fname;
  getline(cin, fname);

  World* world;
  CellFactory cf;
  string str;

  if (fname[0])
    world = new World(fname);
  else {
    int width, height;
    cout << "Enter world size (w h): ";
    cin >> width >> height;
    cout << "Randomize density (0 for nonrandom): ";
    double dens;
    cin >> dens;
    if (dens == 0.0)
      world = new World(width, height);
    else
      world = new World(width, height, dens);

    getline(cin, str);
  }

  cout << "\nEnter 'help' for command reference\n";
  while (true) {
    cout << ">> ";
    getline(cin, str);

    if (str == "") {
      cout << endl;
      continue;
    }

    istringstream cmds(str);

    string cmd;
    cmds >> cmd;
    if (cmd == "cells") {
      for (int i = 0; i < world->get_height(); i++) {
        for (int j = 0; j < world->get_width(); j++)
          cout << world->get_cell_at(i, j);

        cout << endl;
      }
    } else if (cmd == "foods") {
      for (int i = 0; i < world->get_height(); i++) {
        for (int j = 0; j < world->get_width(); j++)
          cout << world->get_food_at(i, j);

        cout << endl;
      }
    } else if (cmd == "cell") {
      int i, j;
      char type;
      int timer;
      cmds >> i >> j >> type >> timer;
      world->put_cell(i, j, cf.make_cell(type, timer));
    } else if (cmd == "tick") {
      int ticks;
      if (!cmds.eof())
        cmds >> ticks;
      else
        ticks = 1;

      for (int i = 0; i < ticks; i++) {
        cout << "Tick " << i + 1 << endl;
        world->tick();
      }
    } else if (cmd == "ticks")
      cout << world->get_ticks() << endl;
    else if (cmd == "food") {
      int i, j, food;
      cmds >> i >> j >> food;
      world->put_food(i, j, food);
    } else if (cmd == "clear") {
      delete world;
      world = new World(world->get_width(), world->get_height());
    } else if (cmd == "exit")
      break;
    else if (cmd == "save") {
      string filename;
      cmds >> filename;
      world->save_to_file(filename);
    } else if (cmd == "help") {
      cout
          << "help                        - show this\n"
          << "exit                        - stop the program\n"
          << "clear                       - make a new empty world\n"
          << "cells                       - show a current world state\n"
          << "foods                       - show a food map of the world\n"
          << "ticks                       - get a number of ticks since world "
             "creation\n"
          << "tick [ticks]                - perform a number of world ticks, "
             "default 1\n"
          << "save <filename>             - save the world to a file\n"
          << "food <i> <j> <amount>       - add food to cell at (i, j)\n"
          << "cell <i> <j> <type> [timer] - add a new cell at (i, j), types:\n"
          << "                              # - classic Conway cell\n"
          << "                                  (lives with 2, 3 neighbors, "
             "born with 3,\n"
          << "                                   eats 1 food unit to not die)\n"
          << "                              * - dynamic cell\n"
          << "                                  (lives with 3, 4 neighbors, "
             "born with 2,\n"
          << "                                   eats 2 food units to not "
             "die)\n"
          << "                              P - producer cell\n"
          << "                                  (lives with 4 neighbors, "
             "born with 4,\n"
          << "                                   eats 4 food units to not "
             "die)\n"
          << "                              T - timer cell with specified "
             "timer\n"
          << "                                  (lives exactly 'timer' ticks,\n"
          << "                                  'timer' propery is ignored for "
             "other cells)\n";
    }

    cout << endl;
  }

  delete world;
  CellPool::dispose();
}