#include "world.hpp"
#include <fstream>
#include <iostream>
#include <random>
#include "cells.hpp"

World::World(int width, int height)
    : width(width),
      height(height),
      ticks_elapsed(0),
      world(height, vector<Cell*>(width, nullptr)),
      world2(height, vector<Cell*>(width, nullptr)),
      foodmap(height, vector<int>(width, 0)),
      cf(new CellFactory()) {
  for (int i = 0; i < height; i++)
    for (int j = 0; j < width; j++) {
      world[i][j] = nullptr;
      world2[i][j] = nullptr;
      foodmap[i][j] = 0;
    }
}

World::World(int width, int height, double density) : World(width, height) {
  // get number of cells to generate
  int cells = width * height * density;
  // initialize random
  std::mt19937 rng;
  rng.seed(std::random_device()());
  std::uniform_int_distribution<std::mt19937::result_type> width_distribution(
      0, width - 1);
  std::uniform_int_distribution<std::mt19937::result_type> height_distribution(
      0, height - 1);
  std::uniform_int_distribution<std::mt19937::result_type> type_distribution(0,
                                                                             2);

  // fill cells
  for (int c = 0; c < cells; c++) {
    // get random position
    int i;
    int j;
    do {
      i = height_distribution(rng);
      j = width_distribution(rng);
    } while (world[i][j] != nullptr);

    int type = type_distribution(rng);
    Cell* cell = cf->make_cell(type);

    world[i][j] = cell;
  }
}

World::World(std::string filename) {
  std::ifstream file(filename);
  file >> width >> height >> ticks_elapsed;
  cf = new CellFactory();

  world.resize(height);
  for (int i = 0; i < world.size(); i++) world[i].resize(width);
  world2.resize(height);
  for (int i = 0; i < world2.size(); i++) world2[i].resize(width);
  foodmap.resize(height);
  for (int i = 0; i < foodmap.size(); i++) foodmap[i].resize(width);

  for (int i = 0; i < height; i++)
    for (int j = 0; j < width; j++) {
      world[i][j] = nullptr;
      world[i][j] = nullptr;
      foodmap[i][j] = 0;
    }

  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      char ch;
      file >> ch;
      world[i][j] = cf->make_cell(ch);
    }
  }

  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      file >> foodmap[i][j];
    }
  }
}

void World::put_cell(int i, int j, Cell* cell) {
  normalize(i, j);
  if (world[i][j] != nullptr && world[i][j]->display() >= '0' &&
      world[i][j]->display() <= '9')
    CellPool::get_instance()->return_timer_cell(
        static_cast<TimerCell*>(world[i][j]));
  world[i][j] = cell;
}

void World::put_food(int i, int j, int food) {
  normalize(i, j);
  foodmap[i][j] += food;
  foodmap[i][j] %= 10;
}

void World::tick() {
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      world2[i][j] = nullptr;              // make empty
      int neighbors = count_around(i, j);  // count neighbors
      if (world[i][j] == nullptr)          // cell is empty, try to spawn
      {
        if (ProducerCell::will_grow(neighbors))
          world2[i][j] = cf->make_cell('P');
        else if (Cell::will_grow(neighbors))
          world2[i][j] = cf->make_cell('C');
        else if (DynamicCell::will_grow(neighbors))
          world2[i][j] = cf->make_cell('D');
      } else {                          // check if cell lives
        world[i][j]->tick(this, i, j);  // tick cell
        if (world[i][j]->will_live(neighbors))
          world2[i][j] = world[i][j];
        else if (foodmap[i][j] >= world[i][j]->eats()) {
          foodmap[i][j] -= world[i][j]->eats();  // consume food and
          world2[i][j] = world[i][j];            // still live
        } else {
          world2[i][j] = nullptr;  // die
          foodmap[i][j] += 1;      // dead cell leaves food behind
        }
      }
    }
  }

  // clean dead cells and copy back
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      if (world2[i][j] == nullptr && world[i][j] != nullptr &&
          world[i][j]->display() >= '0' && world[i][j]->display() <= '9')
        CellPool::get_instance()->return_timer_cell(
            static_cast<TimerCell*>(world[i][j]));

      world[i][j] = world2[i][j];
    }
  }

  ticks_elapsed += 1;
}

int World::count_around(int i, int j) const {
  int cnt = 0;
  for (int a = -1; a <= 1; a++) {
    for (int b = -1; b <= 1; b++) {
      if (a == 0 && b == 0) continue;
      int ni = i + a;
      int nj = j + b;
      normalize(ni, nj);
      if (world[ni][nj] != nullptr) cnt++;
    }
  }
  return cnt;
}

void World::save_to_file(std::string filename) {
  using std::endl;
  std::ofstream file(filename);
  file << width << endl << height << endl << ticks_elapsed << endl;
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      if (get_cell_at(i, j) != ' ')
        file << get_cell_at(i, j) << ' ';
      else
        file << '-' << ' ';
    }
    file << endl;
  }

  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      file << get_food_at(i, j) << ' ';
    }
    file << endl;
  }
}