#pragma once
#include <vector>
class World;

// a regular cell as defined by Conway
class Cell {
 public:
  static bool will_grow(int neighbors);

  virtual bool will_live(int neighbors);
  virtual char display();
  virtual int eats();
  virtual void tick(World* world, int i, int j);

  virtual ~Cell(){};
};

// a static cell that will die in timer ticks
class TimerCell : public Cell {
 private:
  int timer;

 public:
  static bool will_grow(int neighbors);

  TimerCell()  // timer defaults to 5
      : TimerCell(5) {}
  TimerCell(int timer) {
    if (timer > 9)
      this->timer = 9;
    else if (timer < 0)
      this->timer = 0;
    else
      this->timer = timer;
  }
  bool will_live(int neighbors);
  char display();
  int eats();
  void tick(World* world, int i, int j);
  void reset(int timer) {
    if (timer > 9)
      this->timer = 9;
    else if (timer < 0)
      this->timer = 0;
    else
      this->timer = timer;
  }
};

// a cell that dies easier but is born easier
class DynamicCell : public Cell {
 public:
  static bool will_grow(int neighbors);

  bool will_live(int neighbors);
  char display();
  int eats();
};

// a cell that easily dies and hardly born,
// but produces food to up and left each tick
class ProducerCell : public Cell {
 public:
  static bool will_grow(int neighbors);

  bool will_live(int neighbors);
  char display();
  int eats();
  void tick(World* world, int i, int j);
};

// ObjectPool pattern, is also a Singleton
class CellPool {
 public:
  static CellPool* get_instance();
  static void dispose();
  Cell* get_regular_cell();
  DynamicCell* get_dynamic_cell();
  ProducerCell* get_producer_cell();
  TimerCell* get_timer_cell(int timer);
  void return_timer_cell(TimerCell* tc);

  ~CellPool();

 private:
  static CellPool* instance;
  CellPool();

  std::vector<TimerCell*> timer_cells;
  Cell* regular_cell;
  DynamicCell* dynamic_cell;
  ProducerCell* producer_cell;
};

// Factory that creates cells by its id
class CellFactory {
 public:
  Cell* make_cell(char id, int timer = 5);
  Cell* make_cell(int id, int timer = 5);
};